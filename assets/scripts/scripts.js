var queroembrulhar = false;
var queroqueembrulhe = false;

var App = {
    init: function (){

    	$('.anchor').on('click', function(e) {
            e.preventDefault();
    		$("html, body").animate({ scrollTop: 612 }, 500);
    	});

    	$('.queroEmbrulhar').on('click', function(e) {
            e.preventDefault();
            if(queroembrulhar){
                return;
            }

            ga('send','event','dia-das-maes','quero-embulhar','botao');
            queroembrulhar = true;

            setTimeout(function () {
                queroembrulhar = false;
            }, 10000);
    	})
    	$('.naturaEmbrulhe').on('click', function() {
            if(queroqueembrulhe){
                return;
            }

            ga('send','event','dia-das-maes','quero-que-embrulhe','botao');
            queroqueembrulhe = true;

            setTimeout(function () {
                queroqueembrulhe = false;
            }, 10000);
    	});

        $('a.goToVideo').on('click', function(e){
            e.preventDefault();
            $('.videoHolder').fadeOut();
            $('.video-area').animate({height:371},200);
            setTimeout(function() {
                $('.showVideo').fadeIn('slow');
            }, 700);
        });

    }

}

$(function() {
	App.init();
});